;;; init.el --- Summary:

;;; Commentary:

;; This config file designed only for Emacs 29+

;;; Code:

(load-file (expand-file-name "elpaca-init.el" user-emacs-directory))

;; Install or configure packages

;; === Emacs ===

(use-package emacs
  :elpaca nil
  :init

  ;; Allow to read 4mb process output
  ;; Required option for lsp-mode
  (setq read-process-output-max (* 1024 1024 4)) ;; 4mb

  ;; Show scratch buffer on startup
  (setq inhibit-startup-screen t)
  (setq initial-scratch-message ";; You are \"Davay, davay!\" now?\n\n")

  ;; Put a new line at the end of the files
  (setq require-final-newline t)

  ;; When clear line, also kill it
  (setq kill-whole-line t)

  ;; Show trailing whitespaces helps keep code clean
  (setq show-trailing-whitespace t)

  ;; turn off bleeps!
  ;; TODO: test and maybe remove
  (setq visible-bell 1)

  ;; Backups back again!
  (make-directory "~/.emacs_backups/" t)
  (make-directory "~/.emacs_autosave/" t)
  (setq auto-save-file-name-transforms '((".*" "~/.emacs_autosave/" t)))
  (setq backup-directory-alist '(("." . "~/.emacs_backups/")))

  ;; Increase cursor blinking
  (setq blink-cursor-interval 0.3)
  (setq blink-cursor-delay 0.3)
  (setq blink-cursor-blinks 100)

  ;; Using 4 spaces instead tabs
  (setq indent-tabs-mode nil)
  (setq tab-width 4)

  ;; Scroll one line at a time (less "jumpy" than defaults)
  ;; Source: https://github.com/corvideon/mousemacs/blob/3c24c75a25c77af3a03a454d8e0a2978e289f171/mousemacs/mousemacs-core.el#L254C1-L258C62
  ;;(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time
  (setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
  (setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
  (setq scroll-step 3) ;; keyboard scroll a few lines at a time


  ;; turn off mouse mode that interfere
  ;; Source: https://github.com/corvideon/mousemacs/blob/3c24c75a25c77af3a03a454d8e0a2978e289f171/mousemacs/mousemacs-core.el#L271C1-L277C31
  (setq mouse-yank-at-point nil)
  (setq mouse-drag-copy-region nil)
  (setq x-select-enable-primary nil)
  (setq x-select-enable-clipboard t)
  (setq select-active-regions t)

  ;; Something for better performance
  (setq inhibit-compacting-font-caches t)

  ;; Treesit grammars
  (setq treesit-extra-load-path '(concat user-emacs-directory "tree-sitter"))

  ;; start with a maximised window
  (add-to-list 'default-frame-alist '(fullscreen . maximized))

  ;; What we run for text editing stuff?
  :hook ((prog-mode text-mode) . (lambda ()
				   (display-line-numbers-mode 1)
				   (column-number-mode 1)
				   (hl-line-mode 1)
				   (electric-pair-mode 1)))

  ;; What we run after all init done
  :hook (elpaca-after-init . (lambda ()
					;(toggle-frame-maximized)
			       (tool-bar-mode -1)
			       (scroll-bar-mode -1)
					;(mouse-wheel-mode -1)
			       ;; reload a file if changed in external program
			       (global-auto-revert-mode t)

			       ;; turn on word wrap globally
			       (global-visual-line-mode t)

			       ;; No region when it is not highlighted
			       (transient-mark-mode t)))

  :bind (("C-M-i" . completion-at-point)))

(use-package treesit
  :elpaca nil
  :mode (
	 ;; Bash/sh/env
	 (".sh$" . bash-ts-mode)
	 (".bash$" . bash-ts-mode)
	 (".env$" . bash-ts-mode)

	 ;; CMake
	 ("CMakeLists.txt" . cmake-ts-mode)

	 ;; Docker
	 ("Dockerfile*" . dockerfile-ts-mode)
	 ("*.Dockerfile" . dockerfile-ts-mode)

	 ;; Godot
	 (".gdshader$" . glsl-ts-mode)))

;; === CORE ===

;; GCMH
;; Some GC hacks to improve startup speed
(use-package gcmh
  :elpaca t
  :demand t
  :hook (elpaca-after-init . gcmh-mode))

;; Eglot!
;; Configuration for built-int eglot
(use-package eglot
  :elpaca nil
  :config
  ;; Maybe this will improve performance
  ;; BTW, eglot is slow. lsp-server faster but a lot heavier
  (fset #'jsonrpc--log-event #'ignore)
  :hook ((typescript-ts-mode . eglot-ensure)
	 (rust-ts-mode . eglot-ensure)
	 (c++-ts-mode . eglot-ensure)
	 (c-ts-mode . eglot-ensure)
	 (python-ts-mode . eglot-ensure)
	 (csharp-ts-mode . eglot-ensure)
	 (gdscript-ts-mode . eglot-ensure)))

;; Exec path from shell
;; Load environment variables from user's shell
(use-package exec-path-from-shell
  :elpaca t
  :config
  (when (memq window-system '(mac ns x pgtk))
    (exec-path-from-shell-initialize)
    (message "Exec path from shell invoked by you GUI Emacs!"))

  (when (daemonp)
    (exec-path-from-shell-initialize)
    (message "Exec path from shell invoked by you daemon Emacs!")))

;; treesit-auto
;; Automatically enables tree-sitter for all supported modes
(use-package treesit-auto
  :elpaca t
  :demand t
  :config
  (setq treesit-auto-install 'prompt)
  (global-treesit-auto-mode))

;; Ejc SQL
;; Cool sql client (best in Emacs world)
;; Note: Super weird. To use, instal --> 'leiningen' <-- first!
;; Example: pacman -S leiningen
;; My thoughts: it's okay if Emacs is critical. If no, just use BeeKeeper
(use-package ejc-sql
  :elpaca (:host github :repo "kostafey/ejc-sql" :branch "master")
  :after company corfu cape aggressive-indent

  :init
  ;; This is required for ejc-print-cache
  (defun pprint (form &optional output-stream)
    (princ (with-temp-buffer
             (cl-prettyprint form)
             (buffer-string))
           output-stream))
  :hook (ejc-sql-minor-mode . (lambda ()
				(ejc-eldoc-setup)
				(ejc-set-use-unicode t)
				;; Limit of records allowed to fetch
				(ejc-set-fetch-size 500)
				(ejc-set-max-rows 500)

				;; Disable it for better SQL manual formatting
				(aggressive-indent-mode -1)))
  :config
  (use-package ejc-company
    :elpaca nil
    :after cape
    :config
    (push 'ejc-company-backend company-backends)
    ;; Really dirty hack to force company completion works here
    :hook (ejc-sql-minor-mode . (lambda ()
				  (company-mode +1)
				  (corfu-mode -1)
					;(ejc-company-backend)
				  )))
  (setq ejc-result-table-impl 'ejc-result-mode)
  (setq ejc-complete-on-dot t)
  (setq clomacs-httpd-default-port 8121)

  (setq ejc-set-column-width-limit 100)

  ;; Super-important is to setup LATEST available jdbc drivers from Maven
  (setq ejc-jdbc-drivers
	'("sqlite"
	  [org.xerial/sqlite-jdbc "3.23.1"]
	  "h2"
	  [com.h2database/h2 "1.4.199"]
	  "mysql"
	  [mysql/mysql-connector-java "5.1.44"]
	  "postgresql"
	  [org.postgresql/postgresql "42.6.0"]
	  "sqlserver"
	  [com.microsoft.sqlserver/mssql-jdbc "6.2.2.jre8"]
	  "oracle"
	  [com.oracle.jdbc/ojdbc8 "12.2.0.1"]))
  )


;; Nerd icons
;; Library for Nerd icons
(use-package nerd-icons
  :elpaca t)

;; nerd icons dired
;; nerd icons for dired
(use-package nerd-icons-dired
  :elpaca t
  :after nerd-icons
  :hook (dired-mode . nerd-icons-dired-mode))

;; Reverse im
;; Translates keybinds from ANY input methods
(use-package reverse-im
  :elpaca t
  :custom
  (reverse-im-input-methods '("ukrainian-computer" "russian-computer"))
  :config
  (reverse-im-mode 1))

;; Doom themes
;; Set of good themes
(use-package doom-themes
  :elpaca t
  :config
  (load-theme 'doom-tomorrow-night t))

(use-package zenburn-theme
  :elpaca t)

(use-package sculpture-themes
  :elpaca t)

(use-package moe-theme
  :elpaca t)

(use-package color-theme-sanityinc-tomorrow
  :elpaca t)

;; Doom modeline
;; Cool modeline
(use-package doom-modeline
  :elpaca t
  :after nerd-icons
  :config
  (setq doom-modeline-indent-info nil)
  (doom-modeline-mode 1))

;; ESUP
;; Profile Emacs startup!
(use-package esup
  :elpaca t
  :config
  (setq esup-depth 0))

;; Memory usage
;; Analyse Emacs memory usage!
(use-package memory-usage
  :elpaca t)

;; Vundo
;; Visualize undo!
(use-package vundo
  :elpaca t)

;; Agressive indent
;; Agressive indent check
(use-package aggressive-indent
  :elpaca t
  :hook (prog-mode . aggressive-indent-mode))

;; Highlight indentation
;; Visual indent guide
(use-package highlight-indentation
  :elpaca t
  :hook (prog-mode . highlight-indentation-mode))

;; Pulsar
;; Never lose you in code!
(use-package pulsar
  :elpaca t
  :config
  (setq pulsar-pulse t)
  (setq pulsar-delay 0.055)
  :config
  (pulsar-global-mode 1))

;; Treemacs
;; Full-featured sidebar
(use-package treemacs
  :elpaca t
  :bind (("C-x C-n" . treemacs)
	 ("C-c n" . treemacs-select-window))
  :config
  (setq
   treemacs-display-in-side-window          t
   treemacs-eldoc-display                   'simple
   treemacs-file-event-delay                2000
   treemacs-collapse-dirs                   (if treemacs-python-executable 3 0)
   treemacs-deferred-git-apply-delay        0.5
   treemacs-hide-dot-git-directory          t
   treemacs-indentation                     2
   treemacs-show-hidden-files               t
   treemacs-sorting                         'alphabetic-asc
   treemacs-space-between-root-nodes        t
   treemacs-width                           35
   treemacs-width-increment                 1
   treemacs-width-is-initially-locked       nil)

  (treemacs-follow-mode t)
  (treemacs-filewatch-mode t)
  (treemacs-indent-guide-mode 'line)
  (treemacs-git-mode 'deferred)
  (treemacs-fringe-indicator-mode 'always))

;; Treemacs nerd icons
;; Nerd icons for treemacs
(use-package treemacs-nerd-icons
  :elpaca t
  :config
  (treemacs-load-theme "nerd-icons"))

;; Treemacs projectile
;; Projectile support for treemacs
(use-package treemacs-projectile
  :elpaca t)

;; Projectile
;; Easy projects
(use-package projectile
  :elpaca t
  :bind (:map projectile-mode-map
	      ("C-c p" . projectile-command-map))
  :config
  (projectile-mode 1))

;; Yasnippet
;; Snippets framework!
(use-package yasnippet
  :elpaca t
  :config
  (yas-global-mode 1))

;; Yasnippet-snippets
;; Huge collection of yasnippet snippets
;; To learn use: M-x yas-describe-tables
(use-package yasnippet-snippets
  :elpaca t
  :after yasnippet)

;; Vertico
;; Vertical completion UI
(use-package vertico
  :elpaca t
  :init
  (vertico-mode))

;; Marginalia
;; Usefull anotations for M-x menu
(use-package marginalia
  :elpaca t
  :init
  (marginalia-mode))

;; Conslut
;; Usefull commands
(use-package consult
  :elpaca t
  :bind (("C-x b" . consult-buffer)
	 ("C-s" . consult-line)
	 ("C-c s" . consult-ripgrep)))

;; Avy
;; Jump around the text!
(use-package avy
  :elpaca t
  :bind (("C-." . avy-goto-char)
	 ("C-," . avy-goto-char-2)))

;; Popper
;; Easy popup important windows
(use-package popper
  :elpaca t
  :bind (("C-`"   . popper-toggle-latest)
         ("M-`"   . popper-cycle)
         ("C-M-`" . popper-toggle-type))
  :init
  (setq popper-reference-buffers
        '("^\\*eshell.*\\*$" eshell-mode ;eshell as a popup
          "^\\*shell.*\\*$"  shell-mode  ;shell as a popup
          "^\\*term.*\\*$"   term-mode   ;term as a popup
          "^\\*vterm.*\\*$"  vterm-mode  ;vterm as a popup
          "\\*Messages\\*"
          "Output\\*$"
          "\\*Async Shell Command\\*"
          help-mode
          compilation-mode))
  :config
  (popper-mode 1)
  (popper-echo-mode 1))

;; Diff-hl
;; Highlight diff
(use-package diff-hl
  :elpaca t
  :hook ((prog-mode text-mode) . global-diff-hl-mode))

;; Rainbow delimiters
;; Highlihgt parenteses with rainbow
(use-package rainbow-delimiters
  :elpaca t
  :hook (prog-mode . rainbow-delimiters-mode))

;; Editorconfig-charset-extras
;; Additional charsets for editorconfig
(use-package editorconfig-charset-extras
  :elpaca t
  :hook (editorconfig-custom-hooks . editorconfig-charset-extras))

;; magit
;; Best of the best git integration
(use-package magit
  :elpaca t
  :bind ("C-x g" . magit))

;; Corfu
;; Completion framework
(use-package corfu
  :elpaca t
  :init
  (global-corfu-mode)
  (corfu-popupinfo-mode t)
  (corfu-history-mode)
  (corfu-echo-mode)
  :custom
  (corfu-auto t))

;; Nerd-Icons corfu
;; Nerd icons for corfu
(use-package nerd-icons-corfu
  :elpaca (:host github :repo "LuigiPiucco/nerd-icons-corfu" :branch "master")
  :after nerd-icons corfu
  :config
  (add-to-list 'corfu-margin-formatters #'nerd-icons-corfu-formatter))

;; Company
;; Company completions framework.
;; WARNING! Requires only for backends
(use-package company
  :elpaca t)

;; Cape
;; Additions to completion-at-point-functions
(use-package cape
  :elpaca t
  :init
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-elisp-block)
  (add-to-list 'completion-at-point-functions #'cape-elisp-symbol)
  (add-to-list 'completion-at-point-functions #'cape-keyword)
  (add-to-list 'completion-at-point-functions #'cape-emoji)
  :config
  ;; Let's push company backends to CAPF
  (setq-local completion-at-point-functions
	      (mapcar #'cape-company-to-capf
		      (list #'company-files #'company-keywords #'company-dabbrev))))

;; Which key
;; Helper to see keybinds
(use-package which-key
  :elpaca t
  :config
  (which-key-mode 1))

;; ace-window
;; Quickly switch windows!
(use-package ace-window
  :elpaca t
  :bind ("M-o" . ace-window))

;; Winum
;; Highlight window numbers
(use-package winum
  :elpaca t
  :config
  (winum-mode 1))

;; Format all
;; Format buffer with specified formatter
(use-package format-all
  :elpaca t
  :bind ("C-c f" . format-all-region-or-buffer))

;; VLF
;; View large files
(use-package vlf
  :elpaca t)

;; Vdiff
;; Visual diff like vimdiff
(use-package vdiff
  :elpaca t
  :config
  ;; Whether to lock scrolling by default when starting vdiff
  (setq vdiff-lock-scrolling t)

  ;; diff program/algorithm to use. Allows choice of diff or git diff along with
  ;; the various algorithms provided by these commands. See
  ;; `vdiff-diff-algorithms' for the associated command line arguments.
  (setq vdiff-diff-algorithm 'diff))

;; Vterm
;; Better terminal emulator
(use-package vterm
  :elpaca t)

;; Multi-vterm
;; Multi vterm instances
(use-package multi-vterm
  :elpaca t
  :bind (("C-c t" . multi-vterm)))

;; Docker
;; Docker interface for Emacs!
(use-package docker
  :elpaca t)

;; === LANGS ===

;; Pkgbuild mode
;; Support for editing archlinux pkgbuilds
(use-package pkgbuild-mode
  :elpaca t)

;; Docker compose mode
;; Docker-compose files syntax highlighting
(use-package docker-compose-mode
  :elpaca t
  :mode ("docker-compose*.yml"))

;; Caddyfile mode
;; Caddy config files syntax support
(use-package caddyfile-mode
  :elpaca t
  :mode ("Caddyfile.*"
	 "*.Caddyfile"))

;; Gdscript mode
;; Gdscript support
(use-package gdscript-mode
  :elpaca t
  :config
  (setq gdscript-use-tab-indents nil))

;; Lua mode
;; Lua editing support
(use-package lua-mode
  :elpaca t)

;; Typescript mode
;; Typesript editing support
(use-package typescript-mode
  :elpaca t)

;; Rust mode
;; Rust support
(use-package rust-mode
  :elpaca t
  :hook ((rust-mode rust-ts-mode) . (lambda () (setq indent-tabs-mode nil))))

;; === END ===


;; END OF INIT FILE
;; (setq elpaca-after-init-time (current-time)) ;; prevents `elpaca-after-init-hook` from running later.
;; (elpaca-wait)
